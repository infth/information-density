"""
cca.py
-----

module for the calculation of canonical correlations from
different covariance matrices for later use in information
theoretic calculations.
"""
from .base import is_positive_semidefinite
import numpy as np
from numpy.linalg import inv, svd, cholesky, LinAlgError


def cca(sxx, sxy, syy, return_trf=False):
    """ 
    compute the canonical correlations given
    the covariance matrices sxx syy and the cross
    covariance matrix sxy
    """
    # check for positive semidefinite sxx and syy (not perfectly possible with svd
    # due to rounding errors afterwards!)
    if not is_positive_semidefinite(sxx):
        raise np.linalg.LinAlgError("sxx is not positive semidefinite!")

    if not is_positive_semidefinite(syy):
        raise np.linalg.LinAlgError("syy is not positive semidefinite!")

    # calculate the square root of the invers matrix sxx and syy
    uxx, dxx, _ = svd(sxx, hermitian=True)
    uyy, dyy, _ = svd(syy, hermitian=True)

    sxx = uxx @ np.diag(1/np.sqrt(dxx)) @ uxx.T
    syy = uyy @ np.diag(1/np.sqrt(dyy)) @ uyy.T

    if return_trf:
        A, corr, B = svd(sxx @ sxy @ syy)
        return corr, sxx @ A, syy @ B
    else:
        corr = svd(sxx @ sxy @ syy, compute_uv=False)
        return corr

def cov_ext(Z,n):
    """
    Extract the individual covariance matrices
    for a vector (X,Y) namely sxx, sxy, syy given
    the dimension of the random vector X and the
    covariance matrix of Z
    """
    # check Z for symmetry
    if not np.array_equal(Z, Z.T):
        raise LinAlgError('ERROR! >> Nonsymmetric covariance matrix!')

    sxx = Z[0:n, 0:n]
    sxy = Z[0:n, n:]
    syy = Z[n:,n:]
    syx = Z[n:, 0:n]

    # easier to check Z for symmetry but not as verbose as a
    # check on sxx, syy, sxy individually
    # symmetry of sxy is not required nontheless
    # sxy = syx.T due to the symmetry of the covariance
    #if not np.array_equal(sxy, syx.T):
    #    print("WARNING >> sxy != syx.T malformed covariance matrix")
    #if not np.array_equal(sxx, sxx.T):
    #    print("WARNING >> sxx is not symmetric!")
    #if not np.array_equal(syy, syy.T):
    #    print("WARNING >> syy is not symmetric!")
    return sxx, sxy, syy
    
def is_square(m):
    s = m.shape
    if m.ndim == 2 and s[0] == s[1]:
        return True
    return False

if __name__ == '__main__':
    ### test code routines ###
    print('INFO >> Testbench CCA:')

    # cca when the random vectors x and y and
    # their respective covariance and cross covariance 
    # matrices are given
    sxx = np.eye(4)
    sxy = np.diag([.9, .2, .3, .4])
    syy = np.eye(4)

    corr = cca(sxx, sxy, syy)
    print('INFO >> Test 1, Calculated CCA: ', corr)
    # cca when the vector z=(x,y) is given with x=(x1, x1, ..., xn)
    # n = 3 and covariance matrix szz=cov(z) 
    szz = np.array([[1   , 0    , 0   , 0.7 , 0.1  , .4  ],
                    [0   , 1    , 0   , .1  , 0.25 , .2  ],
                    [0   , 0    , 1   , .4  , 0    , 0.3 ],
                    [0.7 , .1   , .4  , 1   , 0    , 0   ],
                    [0.1 , 0.25 , 0   , 0   , 1    , 0   ],
                    [.4  , 0.2    , 0.3 , 0   , 0    , 1   ]])
    corr, A, B = cca(*cov_ext(szz,3), return_trf=True)
    print('INFO >> Test 2, Calculated CCA: ', corr)
    print('INFO |> Calculated Transformation  A: \n', A)
    print('INFO |> Calculated Transformation  B: \n', B)
