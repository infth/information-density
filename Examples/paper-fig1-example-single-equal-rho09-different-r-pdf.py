import sys
sys.path.append("../")

from gaussian import *

import numpy as np
import matplotlib.pyplot as plt
plt.style.use('../mp_style/infth_export.mplstyle')


### generation settings
rho  = .9
nmin = 1
nmax = 5

x = np.linspace(-5.0, 5.0, 10_000)
gi = GaussianInf(rho, max_err=1e-6, center=True)

for i in range(nmin, nmax+1, 1):
     pdf = gi.pdf(x)
     plt.plot(x, pdf, label="$\\varrho_i$={:0.1f}, $r$={:d}".format(rho, gi.s_ccr))
     gi.ccr = np.append(gi.ccr,rho)


plt.ylim((0,1.05))

plt.legend()
plt.xlabel("$x$")
plt.ylabel("$f_{i(\\xi;\\eta)-I(\\xi;\\eta)}(x)$")
plt.savefig("./plots-paper/pdf-rho-09-r-1-5.pdf")
plt.show()
