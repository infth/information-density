import sys
sys.path.append("../")

from gaussian import *

import numpy as np
import matplotlib.pyplot as plt
plt.style.use('../mp_style/infth_export.mplstyle')

### generation settings
#rho = np.array([0.9, 0.9,0.9, 0.9, 0.9,0.9, 0.9,0.9, 0.9, 0.9])


rhoval = 0.2
x = np.linspace(-4, 4, 10_000)

nmax = 6
rho = np.array([])
for i in range(1, nmax, 1):
     rho= np.append(rho,rhoval)

gi = GaussianInf(rho, max_err=1e-6, center=True)
pdf = gi.pdf(x)
plt.plot(x, pdf, label="$\\varrho_i$={:0.1f}, $r$= {:d}".format(gi.ccr[0], gi.s_ccr))

gpdf = gi.gaussian_pdf(x)
plt.plot(x, gpdf, label="$\\varrho_i$={:0.1f}, $r$=  {:d} Gaussian".format(gi.ccr[0], gi.s_ccr))


nmax = 11
rho = np.array([])
for i in range(1, nmax, 1):
     rho= np.append(rho,rhoval)

gi = GaussianInf(rho, max_err=1e-6, center=True)
pdf = gi.pdf(x)
plt.plot(x, pdf, label="$\\varrho_i$={:0.1f}, $r$={:d}".format(gi.ccr[0], gi.s_ccr))

gpdf = gi.gaussian_pdf(x)
plt.plot(x, gpdf, label="$\\varrho_i$={:0.1f}, $r$={:d} Gaussian".format(gi.ccr[0], gi.s_ccr))


nmax = 21
rho = np.array([])
for i in range(1, nmax, 1):
     rho= np.append(rho,rhoval)

gi = GaussianInf(rho, max_err=1e-6, center=True)
pdf = gi.pdf(x)
plt.plot(x, pdf, label="$\\varrho_i$={:0.1f}, $r$={:d}".format(gi.ccr[0], gi.s_ccr))

gpdf = gi.gaussian_pdf(x)
plt.plot(x, gpdf, label="$\\varrho_i$={:0.1f}, $r$={:d} Gaussian".format(gi.ccr[0], gi.s_ccr))


nmax = 41
rho = np.array([])
for i in range(1, nmax, 1):
     rho= np.append(rho,rhoval)

gi = GaussianInf(rho, max_err=1e-6, center=True)
pdf = gi.pdf(x)
plt.plot(x, pdf, label="$\\varrho_i$={:0.1f}, $r$={:d}".format(gi.ccr[0], gi.s_ccr))

gpdf = gi.gaussian_pdf(x)
plt.plot(x, gpdf, label="$\\varrho_i$={:0.1f}, $r$={:d} Gaussian".format(gi.ccr[0], gi.s_ccr))

 
plt.ylim((0,1.1))

plt.legend()
plt.xlabel("$x$")
plt.ylabel("$f_{i(\\xi;\\eta)-I(\\xi;\\eta)}(x)$")
plt.savefig("./plots-paper/pdf-rho-02-r-5-10-20-40-vs-gaussian.pdf")
plt.show()
