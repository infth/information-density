from infth import *
import matplotlib as mp
plt.style.use('./infth/matplotlib/infth_export.mplstyle')

### generation settings

x = np.linspace(-4.25, 4.25, 10_000)

errBound=0.001

#r=5
rho = np.array([0.546665, 0.208497, 0.119926, 0.0887738, 0.0759668])
gi = GaussianInf(rho, max_err=errBound, center=True)
pdf = gi.pdf(x)
plt.plot(x, pdf, label="$r$ = $\,$ {:d} (finite sum, $n$ = 217)".format(gi.s_ccr))


#r=10
rho = np.array([0.649777, 0.355621, 0.216391, 0.154067, 0.121024, 0.101404, 0.0890091, 0.081016, 0.0760076, 0.0732425])
gi = GaussianInf(rho, max_err=errBound, center=True)
pdf = gi.pdf(x)
plt.plot(x, pdf, label="$r$ = {:d} (finite sum, $n$ =  333)".format(gi.s_ccr))


#r=20
rho = np.array([0.727432, 0.534525, 0.376244, 0.279959, 0.220862, 0.182236, 0.155492, 0.136119, 0.121594, 0.110421, 0.101661, 0.0947021, 0.0891291, 0.0846535, 0.08107, 0.0782307, 0.0760282, 0.0743852, 0.0732473, 0.0725782])
gi = GaussianInf(rho, max_err=errBound, center=True)
pdf = gi.pdf(x)
plt.plot(x, pdf, label="$r$ = {:d} (finite sum, $n$ = 462)".format(gi.s_ccr))

gpdf = gi.gaussian_pdf(x)
plt.plot(x, gpdf, label="$r$ = {:d} (Gaussian)".format(gi.s_ccr))


#r=40
rho = np.array([0.774396, 0.680854, 0.567044, 0.466659, 0.38859, 0.329704, 0.285011, 0.250478, 0.223255, 0.201383, 0.183509, 0.168684, 0.156229, 0.145648, 0.136572, 0.128723, 0.121885, 0.115893, 0.110614, 0.105941, 0.101791, 0.0980934, 0.0947908, 0.0918361, 0.0891897, 0.0868185, 0.0846945, 0.0827941, 0.0810972, 0.0795867, 0.0782481, 0.0770689, 0.0760386, 0.0751484, 0.0743908, 0.0737596, 0.0732497, 0.0728571, 0.0725788, 0.0724127])
gi = GaussianInf(rho, max_err=errBound, center=True)
pdf = gi.pdf(x)
plt.plot(x, pdf, label="$r$ = {:d} (finite sum, $n$ = 649)".format(gi.s_ccr))

gpdf = gi.gaussian_pdf(x)
plt.plot(x, gpdf, label="$r$ = {:d} (Gaussian)".format(gi.s_ccr))


plt.ylim((0,1.05))
plt.legend()
plt.xlabel("$x$")
plt.ylabel("$\\hat{f}_{\\mathrm{i}(\\xi;\\eta)-I(\\xi;\\eta)}(x,n)$")
plt.savefig("./plots-paper/pdf-add-white-noise-ar1-input-corr0.9-r-5-10-20-40-vs-gaussian-e001.pdf")
plt.show()


