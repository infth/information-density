import sys
sys.path.append("../")

from gaussian import *

import numpy as np
import matplotlib.pyplot as plt
plt.style.use('../mp_style/infth_export.mplstyle')

x = np.linspace(-5.0, 5.0, 10_000)

### generation settings
rho = np.array([0.9, 0.9,0.9, 0.9,0.9])
gi = GaussianInf(rho, max_err=1e-6, center=True)
pdf = gi.pdf(x)
plt.plot(x, pdf, label="$\\varrho_i$={:0.1f}, $r$={:d}".format(gi.ccr[0], gi.s_ccr))

rho = np.array([0.7, 0.7,0.7, 0.7,0.7])
gi = GaussianInf(rho, max_err=1e-6, center=True)
pdf = gi.pdf(x)
plt.plot(x, pdf, label="$\\varrho_i$={:0.1f}, $r$={:d}".format(gi.ccr[0], gi.s_ccr))

rho = np.array([0.5, 0.5,0.5, 0.5,0.5])
gi = GaussianInf(rho, max_err=1e-6, center=True)
pdf = gi.pdf(x)
plt.plot(x, pdf, label="$\\varrho_i$={:0.1f}, $r$={:d}".format(gi.ccr[0], gi.s_ccr))

rho = np.array([0.2, 0.2,0.2, 0.2,0.2])
gi = GaussianInf(rho, max_err=1e-6, center=True)
pdf = gi.pdf(x)
plt.plot(x, pdf, label="$\\varrho_i$={:0.1f}, $r$={:d}".format(gi.ccr[0], gi.s_ccr))

rho = np.array([0.1, 0.1,0.1, 0.1,0.1])
gi = GaussianInf(rho, max_err=1e-6, center=True)
pdf = gi.pdf(x)
plt.plot(x, pdf, label="$\\varrho_i$={:0.1f}, $r$={:d}".format(gi.ccr[0], gi.s_ccr))


plt.ylim((0,2.30))

plt.legend()
plt.xlabel("$x$")
plt.ylabel("$f_{i(\\xi;\\eta)-I(\\xi;\\eta)}(x)$")
plt.savefig("./plots-paper/pdf-rho-09-07-05-02-01-r5.pdf")
plt.show()
