import sys
sys.path.append("../")

from gaussian import *

import numpy as np
import matplotlib.pyplot as plt
plt.style.use('../mp_style/infth_export.mplstyle')

rhoval = 0.2
x = np.linspace(-4.0, 4.0, 10_000)

nmax = 6
rho = np.array([])
for i in range(1, nmax, 1):
     rho= np.append(rho,rhoval)

gi = GaussianInf(rho, max_err=1e-6, center=True)
cdf = gi.cdf(x)
plt.plot(x, cdf, label="$\\varrho_i$={:0.1f}, $r$= {:d}".format(gi.ccr[0], gi.s_ccr))

gcdf = gi.gaussian_cdf(x)
plt.plot(x, gcdf, label="$\\varrho_i$={:0.1f}, $r$=  {:d} Gaussian".format(gi.ccr[0], gi.s_ccr))


nmax = 11
rho = np.array([])
for i in range(1, nmax, 1):
     rho= np.append(rho,rhoval)

gi = GaussianInf(rho, max_err=1e-6, center=True)
cdf = gi.cdf(x)
plt.plot(x, cdf, label="$\\varrho_i$={:0.1f}, $r$={:d}".format(gi.ccr[0], gi.s_ccr))

gcdf = gi.gaussian_cdf(x)
plt.plot(x, gcdf, label="$\\varrho_i$={:0.1f}, $r$={:d} Gaussian".format(gi.ccr[0], gi.s_ccr))


nmax = 21
rho = np.array([])
for i in range(1, nmax, 1):
     rho= np.append(rho,rhoval)

gi = GaussianInf(rho, max_err=1e-6, center=True)
cdf = gi.cdf(x)
plt.plot(x, cdf, label="$\\varrho_i$={:0.1f}, $r$={:d}".format(gi.ccr[0], gi.s_ccr))

gcdf = gi.gaussian_cdf(x)
plt.plot(x, gcdf, label="$\\varrho_i$={:0.1f}, $r$={:d} Gaussian".format(gi.ccr[0], gi.s_ccr))


nmax = 41
rho = np.array([])
for i in range(1, nmax, 1):
     rho= np.append(rho,rhoval)

gi = GaussianInf(rho, max_err=1e-6, center=True)
cdf = gi.cdf(x)
plt.plot(x, cdf, label="$\\varrho_i$={:0.1f}, $r$={:d}".format(gi.ccr[0], gi.s_ccr))

gcdf = gi.gaussian_cdf(x)
plt.plot(x, gcdf, label="$\\varrho_i$={:0.1f}, $r$={:d} Gaussian".format(gi.ccr[0], gi.s_ccr))

 

plt.ylim((0,1.1))

plt.legend()
plt.xlabel("$x$")
plt.ylabel("$F_{i(\\xi;\\eta)-I(\\xi;\\eta)}(x)$")
plt.savefig("./plots-paper/cdf-rho-02-r-5-10-20-40-vs-gaussian.pdf")
plt.show()
