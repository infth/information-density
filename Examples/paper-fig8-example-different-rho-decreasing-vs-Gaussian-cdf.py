import sys
sys.path.append("../")

from gaussian import *

import numpy as np
import matplotlib.pyplot as plt
plt.style.use('../mp_style/infth_export.mplstyle')

### generation settings

# cca values
# 0.537029, 0.207584, 0.126304, 0.0905719, 0.0705592, 
# 0.0577778, 0.0489121, 0.0424031, 0.037422, 0.0334875, 
# 0.0303013, 0.0276685, 0.0254565, 0.023572, 0.0219471, 
# 0.0205318, 0.0192879, 0.0181861, 0.0172034, 0.0163214

x = np.linspace(-2.25, 2.25, 10_000)    

rho = np.array([0.537029, 0.207584])
gi = GaussianInf(rho, max_err=0.01, center=True)
cdf = gi.cdf(x)
plt.plot(x, cdf, label="$r$ = $\,$ {:d} (finite sum, $n$ =  20)".format(gi.s_ccr))


rho = np.array([0.537029, 0.207584, 0.126304, 0.0905719, 0.0705592])
gi = GaussianInf(rho, max_err=0.01, center=True)
cdf = gi.cdf(x)
plt.plot(x, cdf, label="$r$ = $\,$ {:d} (finite sum, $n$ =  196)".format(gi.s_ccr))



rho = np.array([0.537029, 0.207584, 0.126304, 0.0905719, 0.0705592, 0.0577778, 0.0489121, 0.0424031, 0.037422, 0.0334875])
gi = GaussianInf(rho, max_err=0.01, center=True)
cdf = gi.cdf(x)
plt.plot(x, cdf, label="$r$ = {:d} (finite sum, $n$ =  886)".format(gi.s_ccr))



rho = np.array([0.537029, 0.207584, 0.126304, 0.0905719, 0.0705592, 0.0577778, 0.0489121, 0.0424031, 0.037422, 0.0334875, 0.0303013, 0.0276685, 0.0254565, 0.023572, 0.0219471])
gi = GaussianInf(rho, max_err=0.01, center=True)
cdf = gi.cdf(x)
plt.plot(x, cdf, label="$r$ = {:d} (finite sum, $n$ =  2071)".format(gi.s_ccr))



gcdf = gi.gaussian_cdf(x)
plt.plot(x, gcdf, label="$r$ = {:d} (Gaussian)".format(gi.s_ccr))


plt.ylim((0,1.05))

plt.legend()
plt.xlabel("$x$")
plt.ylabel("$\\hat{F}_{i(\\xi;\\eta)-I(\\xi;\\eta)}(x,n)$")
plt.savefig("./plots-paper/cdf-decreasing-rho-ou-awgn-noise-r2-5-10-15-vs-gaussian-e01.pdf")
plt.show()
