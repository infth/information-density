import warnings

import numpy as np
from numpy.random import chisquare

from scipy.stats import norm
from scipy.special import gamma, beta, kv, modstruve as lv

class GaussianInf:
    """ 
    Calculate the Distribution of the
    Information Density between Gaussian Random Vectors
    with only pairwise dependent components

    for general gaussian random vectors call cca first to
    get an appropriate random vector for use in GaussianInf

    Properties:
     - ccr                 : Canonical Correlations
     - x                   : x coordinates
     - mutual_information  : Mutual Information 
     - expectation         : Mutual Information
     - variance            : variance of the Information Density
     - pdf_error           : returns the calculated pdf error
     - cdf_error           : returns the calculated cdf error

    Methods:
     - pdf                 : returns the probability density function
     - cdf                 : returns the cumulative distribution function
     - both                : returns the pdf and the cdf simultaneously
     - monte_carlo_pdf     : returns samples of the information density distribution
     - gaussian_pdf        : returns an approximate gaussian pdf
     - gaussian_cdf        : returns an approximate gaussian cdf
     - gaussian_both       : returns an approximate gaussian pdf and cdf

    """
    def __init__(self, ccr, max_err=1e-5, itr=None,  max_itr=10_000_000, center=False):
        self._center = center
        self._x = None
        self.ccr = ccr
        self.max_err = max_err
        self.itr = itr
        self.max_itr = max_itr
        self.sdk = None
        
    @property
    def ccr(self):
        return np.repeat(np.append(self.min_r, self._ccr), 
                         np.append(self.s_ccr-np.sum(self.n_ccr), self.n_ccr))
    
    @ccr.setter
    def ccr(self, ccr):
        ccr = np.array(ccr)
        if not ccr.dtype == np.float or not (np.all(ccr > 0) and np.all(ccr < 1)):
            raise ValueError('Wrong or Singular Correlation Values in ccr')
        x = self.x
        self._ccr, self.n_ccr = np.unique(ccr, return_counts=True)
        self.s_ccr=np.sum(self.n_ccr)
        self.min_r, self._ccr = self._ccr[0], self._ccr[1:]
        self.n_ccr = self.n_ccr[1:]
        self.c0 = 1/(self.min_r*np.sqrt(np.pi))
        self.c1 = np.product(np.power(self.min_r/self._ccr, self.n_ccr))
        self.x = x 
        print("INFO: >> ccr=", self.ccr)

    @property
    def x(self):
        if np.all(self._x) != None:
            self._x = np.append(self.zb*[0], self._x)
            return self.xneg*self.min_r*(self._x[self.xinv])+self.mi
        else:
            return None

    @x.setter
    def x(self, x):
        if np.all(x) != None:
            self.mi = (not self._center)*self.mutual_information
            self._x = x-self.mi
            self.xneg = np.where(self._x < 0, -1, 1)
            # remove negative and positive duplicates 
            self._x = np.abs(self._x)/self.min_r
            self._x, self.xinv = np.unique(self._x, return_inverse=True)
            # remove a zero values if one is existent
            self.zb = int(np.any(self._x == 0))
            self._x = self._x[self.zb:]
            
            self.nx = np.size(self._x)
        else: self._x = None

    @x.deleter
    def x(self):
        self._x = None

    @property
    def mutual_information(self):
        return -1/2*np.sum(np.log(1-self.ccr**2))

    @property
    def expectation(self):
        return self.mutual_information()

    @property
    def variance(self):
        return self.central_moment(2)
        
    @property
    def pdf_error(self):
        if self.n_ccr.size > 0 and self.sdk != None:
            return self.calc_pdf_err(self._itr)
        else:
            return None 
        
    @property
    def cdf_error(self):
        if self.n_ccr.size > 0 and self.sdk != None:
            return self.calc_cdf_err(self._itr)
        else:
            return None

    def calc_pdf_err(self, i):
        return self.c0*self.pdf_max(i)*np.abs((1-self.c1*self.sdk))

    def calc_cdf_err(self, i):
        return np.abs((1-self.c1*self.sdk))
    
    def gen_dk(self, err_type):
        """ Calculate DeltaK """
        print("INFO: >> generating delta_k ...")
        self.dk = np.zeros(self.max_itr)
        if self.itr != None:
            cond = lambda i: i >= self.itr
        else: 
            cond = lambda i: self.max_err >= error(i)
        i = 1
        self.sdk = 1
        if err_type.lower() == "pdf":
            error = self.calc_pdf_err
        elif err_type.lower() == "cdf":
            error = self.calc_cdf_err
        elif err_type.lower() == "both":
            if self.c0*self.pdf_max(self.max_itr) < 1:
                error = self.calc_cdf_err
            else:
                error = self.calc_pdf_err
        self.gk = np.power((1-(self.min_r/self._ccr)**2),
                      np.tile(np.arange(1, self.max_itr), (self.n_ccr.size,1)).T)
        self.gk = np.sum(self.n_ccr/2*self.gk, axis=1)
        self.dk[0] = 1

        for i in range(1, self.max_itr):
            if i % 112 == 0: 
                print("INFO: |> pdf_error={:5.5e}, cdf_error={:5.5e} iterations needed: {:,d}"\
                      .format(self.calc_pdf_err(i), self.calc_cdf_err(i),i), end='\r')

            self.dk[i] =  1/i*np.sum(self.gk[0:i] * self.dk[i-1::-1])
            self.sdk += self.dk[i]
            if cond(i):
                break
            elif self.dk[i] == 0:
                warnings.warn("dk equals 0. The sum error will stay constant!")
                break
        else:
            warnings.warn("Maximum iteration limit reached but the specified " 
                           "error wasn't reached with this limit!", RuntimeWarning)
        self._itr = i
        self.dk = self.dk[0:self._itr+1]
        print("INFO: |> pdf_error={:5.5e}, cdf_error={:5.5e}, iterations needed: {}"\
              .format(self.calc_pdf_err(i), self.calc_cdf_err(i),i))
        
    def calc_pdf(self):
        uk0 = ((self._x/2)**((self.s_ccr-1)/2)
              *kv((self.s_ccr-1)/2, self._x)/gamma(self.s_ccr/2))
        self._pdf = np.copy(uk0)

        if self.s_ccr == 1:
            self._pdfz = np.inf
            gk0 = 0
        else:
            gk0 = beta((self.s_ccr-1)/2, .5)/(2*np.sqrt(np.pi))
            self._pdfz = gk0

        if self.n_ccr.size > 0:
            self.gen_dk(err_type="pdf")
            ### ausgabe (can be deleted)
            print("INFO: >> calculate pdf ...")
            print("INFO: |> special function lookup ...")
            k=1
            #### ausgabe 
            uk1 = ((self._x/2)**((self.s_ccr+1)/2)
                      *kv((self.s_ccr+1)/2, self._x)/gamma(self.s_ccr/2+1))
            self._pdf += self.dk[1]*uk1

            gk0 = (self.s_ccr-1)/self.s_ccr*gk0
            self._pdfz = self._pdfz + self.dk[1]*gk0

            for k in range(2, self._itr):
                if k%112 == 0: print("INFO: |> calculate iteration: {:,d}/{:,d}".format(k+1, self._itr), end='\r')
                uk0, uk1 = uk1, (self._x**2/((self.s_ccr+2*k-2)*(self.s_ccr+2*k-4))*uk0+
                                (self.s_ccr+2*k-3)/(self.s_ccr+2*k-2)*uk1)
                self._pdf = self._pdf + self.dk[k]*uk1

                gk0 = (self.s_ccr+2*k-3)/(self.s_ccr+2*k-2)*gk0
                self._pdfz = self._pdfz + self.dk[k]*gk0
            print("INFO: |> calculate iteration: {:,d}/{:,d}".format(k+1, self._itr), end='\r')
        self._pdf *= self.c0*self.c1
        self._pdfz *= self.c0*self.c1
                
    def calc_cdf(self):
        print("INFO: >> calculate cdf ...")
        print("INFO: |> special function lookup ...")
        # for s_ccr > 66 and _x > 697 the initial cdf values don't converge anymore!
        with warnings.catch_warnings() as w:
            warnings.filterwarnings("ignore" ,category=RuntimeWarning)
            self._cdf = (kv((self.s_ccr-1)/2, self._x)*lv((self.s_ccr-3)/2, self._x)+
                         kv((self.s_ccr-3)/2, self._x)*lv((self.s_ccr-1)/2, self._x))
            self._cdfz = 0.0

        if np.any(np.isnan(self._cdf)):
            self._cdf = np.nan_to_num(self._cdf, nan=1/self._x)
            warnings.warn("The supplied x values are too big to be correctly calculated by "
                          "numpy! Some values are therefore changed to static values. "
                          "Nevertheless this might lead to partially wrong results!",
                          RuntimeWarning)
        # possible workaround to ensure correct values in case of self._cdf -> 0
        zidx = np.where(self._cdf == 0)
        self._cdf[zidx] = 1/self._x[zidx]
        
        if self.n_ccr.size > 0:
            self.gen_dk("cdf")
            tl0 = ((self._x/2)**((self.s_ccr-1)/2)*kv((self.s_ccr-1)/2, self._x)/
                   (np.sqrt(np.pi)*gamma(self.s_ccr/2+1)))
            tl1 = ((self._x/2)**((self.s_ccr-1)/2+1)*kv((self.s_ccr-1)/2+1, self._x)/
                   (np.sqrt(np.pi)*gamma(self.s_ccr/2+2)))
            sk = tl0 + tl1
            self._cdf -= self.c1*(self.dk[1]*tl0+self.dk[2]*sk)
            for k in range(2, self._itr-1):
                if k%112 == 0: print("INFO: |> calculate iteration: {:,d}/{:,d}".format(k+1, self._itr), end='\r')
                tl0, tl1 = tl1, (self._x**2*tl0/((self.s_ccr+2*k)*(self.s_ccr+2*k-2))+
                                (self.s_ccr-3+2*k)/(self.s_ccr+2*k)*tl1)
                sk += tl1
                self._cdf -= self.c1*self.dk[k+1]*sk
            print("INFO: |> calculate iteration: {:,d}/{:,d}".format(k+1, self._itr), end='\r')
        self._cdf *= self._x/2

    def calc_both(self):
        print("INFO: >> calculate both ...")
        # for s_ccr > 66 and _x > 697 the initial cdf values don't converge anymore!
        print("INFO: |> special function lookup ...")
        with warnings.catch_warnings() as w:
            warnings.filterwarnings("ignore" ,category=RuntimeWarning)
            self._cdf = (kv((self.s_ccr-1)/2, self._x)*lv((self.s_ccr-3)/2, self._x)+
                         kv((self.s_ccr-3)/2, self._x)*lv((self.s_ccr-1)/2, self._x))
            self._cdfz = 0.0

        if np.any(np.isnan(self._cdf)):
            self._cdf = np.nan_to_num(self._cdf, nan=1/self._x)
            warnings.warn("The supplied x values are too big to be correctly calculated by "  +
                          "numpy! Some values are therefore changed to static values. " +
                          "Nevertheless this might lead to wrong results! ",
                          RuntimeWarning)
        # possible workaround to ensure correct values in case of self._cdf -> 0
        # which is wrong. 
        z_idx = np.where(self._cdf == 0)
        self._cdf[z_idx] = 1/self._x[z_idx]

        uk0 = ((self._x/2)**((self.s_ccr-1)/2)
               *kv((self.s_ccr-1)/2, self._x)/gamma(self.s_ccr/2))
        self._pdf = np.copy(uk0)

        if self.s_ccr == 1:
            self._pdfz = np.inf
            gk0 = 0
        else:
            gk0 = beta((self.s_ccr-1)/2, .5)/(2*np.sqrt(np.pi))
            self._pdfz = gk0

        if self.n_ccr.size > 0:
            self.gen_dk(err_type="both")
            sk = uk0/(np.sqrt(np.pi)*self.s_ccr/2)
            self._cdf -= self.c1*self.dk[1]*sk
            uk1 = ((self._x/2)**((self.s_ccr+1)/2)*\
                      kv((self.s_ccr+1)/2, self._x)/gamma(self.s_ccr/2+1))
            sk = sk + uk1/(np.sqrt(np.pi)*(self.s_ccr/2+1))
            self._pdf += self.dk[1]*uk1

            gk0 = (self.s_ccr-1)/self.s_ccr*gk0
            self._pdfz = self._pdfz + self.dk[1]*gk0

            for k in range(2, self._itr):
                if k%112 == 0: print("INFO: |> calculate iteration: {:,d}/{:,d}".format(k+1, self._itr), end='\r')
                self._cdf -= self.c1*self.dk[k]*sk
                uk0, uk1 = uk1, (self._x**2/((self.s_ccr+2*k-2)*(self.s_ccr+2*k-4))*uk0+
                                (self.s_ccr+2*k-3)/(self.s_ccr+2*k-2)*uk1)
                sk = sk + uk1/(np.sqrt(np.pi)*(self.s_ccr/2+k))
                self._pdf += self.dk[k]*uk1

                gk0 = (self.s_ccr+2*k-3)/(self.s_ccr+2*k-2)*gk0
                self._pdfz = self._pdfz + self.dk[k]*gk0

            print("INFO: |> calculate iteration: {:,d}/{:,d}".format(k+1, self._itr), end='\r')
        self._pdf *= self.c0*self.c1        
        self._pdfz *= self.c0*self.c1
        self._cdf *= self._x/2               

    def pdf(self, x):
        self.x = x
        self.sdk = None
        self.calc_pdf()
        self._pdf = np.append(self.zb*[self._pdfz], self._pdf)
        return self._pdf[self.xinv]

    def cdf(self, x):
        self.x = x
        self.sdk = None
        self.calc_cdf()
        self._cdf = np.append(self.zb*[self._cdfz], self._cdf)
        return 0.5+self.xneg*self._cdf[self.xinv]

    def both(self, x):
        self.x = x
        self.calc_both()
        self._pdf = np.append(self.zb*[self._pdfz], self._pdf)
        self._cdf = np.append(self.zb*[self._cdfz], self._cdf)
        return (self._pdf[self.xinv], 
                0.5+self.xneg*self._cdf[self.xinv])

    def pdf_max(self, k):
        """
        calculate the maximum of the pdf which is attained at x=0
        """
        # for too large itr beta function gets too inaccurate
        if k > 700: k = 700
        # use beta functions instead of gamma functions to prevent overflow
        return beta((self.s_ccr-1)/2+k, .5)/(2*np.sqrt(np.pi))

    def random(self, samples=10_000):
        """
        Generate random  samples of the information density using a 
        Monte Carlo method. The results might be Plotted in 
        a Histogram and can used to verify the numerical approach.
        """
        cs = chisquare(1, (2, self.s_ccr, samples))
        return self.ccr/2 @ (cs[0] - cs[1]) + self.mi

    def monte_carlo_pdf(self, samples=10_000, x=None, bins=None, xloc="center"):
        """
        Samples the PDF of the information density using a 
        Monte Carlo method. The results might be Plotted in 
        a Histogram and can used to verify the numerical approach.

        If an x vector is supplied the pdf is cropped! This is useful if one
        wants to compare the finite sum approximation against a monte carlo simulation.
        """
        if np.all(x) != None:
            xmin = np.min(x)
            xmax = np.max(x)

            if bins == None:
                bins = x.size
            hist, x = np.histogram(self.random(samples), range=(xmin, xmax), bins=bins, density=True)
            
        else:
            hist, x = np.histogram(self.random(samples), bins=bins, density=True)

        if xloc == "center":
            x = (x[1:]+x[:-1])/2
        elif xloc == "left":
            x = x[:-1]
        elif xloc == "right":
            x = x[1:]
            
        return x, hist

    def monte_carlo_cdf(self, samples=None, x=None, bins=1000, xloc="center"):
        """
        Uses the monte_carlo_pdf method to sum the elements along 
        a given axis.
        """
        x, hist = self.monte_carlo_pdf(samples, x, bins, xloc)
        return x, np.cumsum(hist/np.sum(hist))
        

    def gaussian_pdf(self, x):
        self.x = x
        return norm.pdf(self.x, loc=self.mi, scale=np.sqrt(self.variance))

    def gaussian_cdf(self, x):
        self.x = x
        return norm.cdf(self.x, loc=self.mi, scale=np.sqrt(self.variance))

    def gaussian_both(self, x):
        self.x = x
        return (norm.pdf(self.x, loc=self.mi, scale=np.sqrt(self.variance)),
                norm.cdf(self.x, loc=self.mi, scale=np.sqrt(self.variance)))

    def __repr__(self):
        if self.itr == None:
            return f'GaussianInf(ccr={self.ccr}, max_err={self.max_err})'
        else:
            return f'GaussianInf(ccr={self.ccr}, itr={self.itr})'



if __name__ == '__main__':
    pass
    # test code goes here!
    
