# Distribution of the Information Density of Gaussian Random Vectors

This repository contains supplementary material, examples and the source code for the
Paper "On the Distribution of the Information Density of Gaussian Random Vectors: Explicit Formulas and Tight Approximations" (Jonathan Huffmann and Martin Mittelbach, [arXiv:2105.03925](https://arxiv.org/abs/2105.03925))


## Requirements
The code has been developed and tested under:
- Python 3.8

with the modules:
- numpy 1.20
- scipy 1.5

for plotting the additional module:
- matplotlib 3.3

to use the suppemental Notebook:
- Jupyter 1.0

## Binder
To run the code in your browser use:
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/infth%2Finformation-density/main?labpath=.%2Finformation-density-demo.ipynb)

## Referencing
You can use the following BibTeX entry for this work

```bibtex
@article{infth2021,
	author = "J. Huffmann, M. Mittelbach", 
	title = "On the Distribution of the Information Density of Gaussian Random Vectors: Explicit Formulas and Tight Approximations",
	institution = "arXiv",
	year = "2021",
	month = "May",
}
```


## License
This program is licensed under the GPLv3 license. If you in any way use this
code for research that results in publications, please cite our original
article listed above.
