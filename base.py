import numpy as np
from numpy.linalg import cholesky, LinAlgError, eigvalsh

def is_hermitian(arr):
    """ Checks whether a given real matrix is symmetric
        or a given complex matrix is hermitian.
    """
    if np.allclose(arr, np.conj(arr.T)):
        return True
    else:
        return False

def is_positive_semidefinite(arr, atol=1e-8):
    """
    Test symmetric matrix A of positive semidefiniteness.
    """
    e = eigvalsh(arr)
    if is_hermitian(arr):
        if np.any(e < -np.abs(atol)):
            return False
        else:
            return True
    else:
        return False
    
def is_positive_definite(arr):
    """
    Test symmetric matrix A of positive definiteness.

    solution from stackoverflow:
    https://stackoverflow.com/questions/16266720/
    find-out-if-matrix-is-positive-definite-with-numpy
    """
    if is_hermitian(arr):
        try:
            cholesky(arr)
            return True
        except LinAlgError:
            return False
    else:
        return False
